package com.the.sample.app.model

import org.springframework.data.cassandra.core.mapping.PrimaryKey
import org.springframework.data.cassandra.core.mapping.Table


@Table
class User (
    @PrimaryKey
    var id: String? = null,
    var fullName: String,
    var email: String
)