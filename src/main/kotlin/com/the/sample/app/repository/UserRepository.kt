package com.the.sample.app.repository

import com.the.sample.app.model.User
import org.springframework.data.cassandra.repository.ReactiveCassandraRepository
import org.springframework.stereotype.Repository
import reactor.core.publisher.Mono

@Repository
interface UserRepository : ReactiveCassandraRepository<User, String> {
    fun findByEmail(email: String?): Mono<User>
}